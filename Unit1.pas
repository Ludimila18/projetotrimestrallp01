unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TTAplicativos = class(TForm)
    butInserir: TButton;
    ButDel: TButton;
    butSalvar: TButton;
    butAtu: TButton;
    butCarregar: TButton;
    lbAplicativos: TListBox;
    eDesenvolvedor: TEdit;
    eData: TEdit;
    eNome: TEdit;
    procedure butInserirClick(Sender: TObject);
    procedure ButDelClick(Sender: TObject);
    procedure butSalvarClick(Sender: TObject);
    procedure butAtuClick(Sender: TObject);
    procedure butCarregarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  Ttaplicativo = class(TObject)
    nome, desenvolvedor, data: string;
  end;

var
  TAplicativos: TTAplicativos;

implementation

{$R *.dfm}

procedure TTAplicativos.butAtuClick(Sender: TObject);
var aplicativo:TtAplicativo;
var nome, desenvolvedor, data: string;
begin
  nome:=eNome.Text;
  desenvolvedor:=eDesenvolvedor.Text;
  data:=eData.Text;
  if (nome.Length<>0) and (desenvolvedor.Length<>0) and (data.Length<>0) then
  begin
    aplicativo := lbAplicativos.Items.Objects[lbAplicativos.ItemIndex] as Ttaplicativo;
    aplicativo.nome := nome;
    aplicativo.desenvolvedor := desenvolvedor;
    aplicativo.data := data;
    lbAplicativos.Items[lbAplicativos.ItemIndex]:=eNome.Text ;
  end
  else
  begin
    ShowMessage('N�o deixe campos em branco');
  end;
end;

procedure TTAplicativos.butCarregarClick(Sender: TObject);
var arqApp:TextFile;
var aplicativo:Ttaplicativo;
begin
  AssignFile(arqApp, 'Aplicativos.txt');
  Reset(arqApp);

  aplicativo:=Ttaplicativo.Create;

  while not Eof(arqApp) do
  begin
      Readln(arqApp, aplicativo.nome);
      Readln(arqApp, aplicativo.desenvolvedor);
      Readln(arqApp, aplicativo.data);

      lbAplicativos.Items.AddObject(aplicativo.nome, aplicativo);
  end;
    CloseFile(arqApp);
end;

procedure TTAplicativos.ButDelClick(Sender: TObject);
begin
lbAplicativos.DeleteSelected;
end;

procedure TTAplicativos.butInserirClick(Sender: TObject);
var aplicativos:Ttaplicativo;
var nome, desenvolvedor, data: string;
begin
aplicativos:=Ttaplicativo.Create;
aplicativos.nome:=eNome.Text;
aplicativos.desenvolvedor:=eDesenvolvedor.Text;
aplicativos.data:=eData.Text;
if (aplicativos.nome <> '') and (aplicativos.desenvolvedor <> '') and (aplicativos.data <> '')then
begin
lbAplicativos.items.AddObject(aplicativos.nome, aplicativos);
eNome.text:=nome;
eDesenvolvedor.text:= desenvolvedor;
eData.text:=data;
end
 else
begin
ShowMessage('Campo vazio');
end;
end;

procedure TTAplicativos.butSalvarClick(Sender: TObject);
var arqApp : TextFile;
var aplicativo:Ttaplicativo;
var i: integer;
begin
      if lbAplicativos.Items.Count<>0 then
      begin
      AssignFile(arqApp, 'Aplicativos.txt');
      Rewrite(arqApp);
      for i:=0 to Pred(lbAplicativos.Items.Count) do
      begin
        aplicativo:= lbAplicativos.Items.Objects[i] as Ttaplicativo;
        writeln(arqApp, aplicativo.nome);
        writeln(arqApp, aplicativo.desenvolvedor);
        writeln(arqApp, aplicativo.data);
      end;

      CloseFile(arqApp);
    end
    else
    begin
      ShowMessage('N�o tem nada para salvar!');
    end;
end;
end.
